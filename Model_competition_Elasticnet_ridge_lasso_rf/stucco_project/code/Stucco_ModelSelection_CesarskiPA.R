## ---------------------------------------------------------------------------
rm(list=ls()) 
library(rstudioapi)
setwd(dirname(rstudioapi::getSourceEditorContext()$path))


## ---------------------------------------------------------------------------
library(ISLR)
library(caret)
library(MLmetrics)
library(MASS)
library(glmnet)


## ---------------------------------------------------------------------------
#load data
data.dir = "../data/"       #data directory
data.file = "stucco.csv"
stucco <- read.csv(paste(data.dir,data.file,sep=""), 
                         stringsAsFactors = TRUE)
###A test set is also provided
test.data.file = "stucco_test.csv"
test.data <- read.csv(paste(data.dir,test.data.file,sep=""), 
                   stringsAsFactors = TRUE)



## ---------------------------------------------------------------------------
dim(stucco)   # How many rows and columns?
names(stucco) # What are the names of the columns?
head(stucco)  # Show me a few lines
summary(stucco)
sum(is.na(stucco))  #any missing data/NA  ##No missing data in set-POLARIS
###also check test set
dim(test.data)   # How many rows and columns?
names(test.data) # What are the names of the columns?
head(test.data)  # Show me a few lines
summary(test.data)
sum(is.na(test.data))
###Test set looks OK



## ---------------------------------------------------------------------------
# setting seed to generate a  reproducible random sampling 
###set.seed(248) - not needed (see beow)

###POLARIS:
#*A test set is provided in separate data file
#*if I cross validate, I will not need to split the data at all
#*So, I will cross validate against the stucco set and not worry  about seeds or splitting data

#*the code below might seem redundant but it allows me to drop in some your code
#*I don't consider this to be "just creepypasta"
#*I know what it's like to read somebody else's code and I'm hoping to make the project readable
#*...and (of Course) repurposing some of your code also makes my life easier.  
#*I'm tweaking it though
train_data  <- stucco 
test_data <- test.data


## ---------------------------------------------------------------------------
#training
dim(train_data)   
head(train_data)  
summary(train_data)

#test
dim(test_data)   
head(test_data)  
summary(test_data)


## ---------------------------------------------------------------------------

set.seed(593)
#inputs to the caret function.  we are doing repeated k-fold cross validation
#10 folds. repeat 5 times
num_folds = 5 
num_repeats = 3
train.control <- caret::trainControl(
        method = "repeatedcv", 
        number = num_folds,
        repeats = num_repeats,
        savePredictions = TRUE,
        index = createMultiFolds(train_data$strength, k=num_folds, times=num_repeats) 
)




## ---- warning=FALSE---------------------------------------------------------

#linear model using all data
cv.mod.lin <- caret::train(strength ~., data = train_data,
                              method ="lm", trControl = train.control)
#caret returns CV RMSE
cv.mod.lin $results$RMSE 

#interactions
cv.mod.int <- caret::train(strength ~.^2, data = train_data,
                           method ="lm", trControl = train.control)
cv.mod.int$results$RMSE

#quadratic
#*Looks like the code below omits any categoric variables.  Let's do that...
#*we need to pull out "method","mixing","region"

quad_mod = as.formula(
        paste('strength ~',
              paste(
                      paste(c("method","mixing","region"),collapse = ' + '),
                      paste('poly(',colnames(subset(stucco, select=-c(strength,method,mixing,region)))
                            ,',2,raw=TRUE)',
                            collapse = ' + '),sep = ' + ')))

cv.mod.quad <- caret::train(quad_mod, data = train_data,
                            method ="lm", trControl = train.control)
cv.mod.quad$results$RMSE 




## ---------------------------------------------------------------------------
#AIC stepwise on linear model with all variables
cv.mod.lin.all.aic <- caret::train(strength ~., data = train_data,
                                   method ="lmStepAIC",trControl = train.control,trace = FALSE)
cv.mod.lin.all.aic$results$RMSE



## ---- warning=FALSE---------------------------------------------------------

#####
# regularization using ridge and lasso and elastic net
# here we will just do elasticnet since ridge/lasso are special cases
#

#grid of alpha nad lambda to consider for elastic net 
lgrid <- 10^seq(6,-3,length=25)
agrid <- seq(0,1,length=4)


#linear model
ela.lin <- caret::train(strength ~., data = train_data,
                        method = "glmnet", trControl = train.control,
                        tuneGrid = expand.grid(alpha =agrid, lambda=lgrid))
#best cv RMSE, alpha, lambda
c(min(ela.lin$results$RMSE),ela.lin$bestTune )

#interaction model
ela.int <- caret::train(strength ~.^2, data = train_data,
                        method = "glmnet", trControl = train.control,
                        tuneGrid = expand.grid(alpha =agrid, lambda=lgrid))
c(min(ela.int$results$RMSE),ela.int$bestTune )


#quadratic model
ela.quad <- caret::train(quad_mod, data = train_data,
                        method = "glmnet", trControl = train.control,
                        tuneGrid = expand.grid(alpha =agrid, lambda=lgrid))
c(min(ela.quad$results$RMSE),ela.quad$bestTune )




###*I want to try re-running all of these with some onehot encoding
###*use caret to do one hot encoding for all categoric variables
###* using 'dummyvars'
###* 

dmy <- dummyVars(" ~ .", data = stucco)
stucco.onehot <- data.frame(predict(dmy, newdata = stucco))

names(stucco.onehot)
###Looks good
###harmonize variables to make the next part understandable
train_data.onehot <- stucco.onehot





###-----------------------------------------------------------------------------------------
###Repeat tests with one hot population
###
## ---------------------------------------------------------------------------

###I will NEED another TrainControl object even though we only use one in HW

set.seed(593)
#inputs to the caret function.  we are doing repeated k-fold cross validation
#10 folds. repeat 5 times
num_folds = 5 
num_repeats = 3
train.control.onehot <- caret::trainControl(
  method = "repeatedcv", 
  number = num_folds,
  repeats = num_repeats,
  savePredictions = TRUE,
  index = createMultiFolds(train_data.onehot$strength, k=num_folds, times=num_repeats) 
)




## ---- warning=FALSE---------------------------------------------------------

#linear model using all data
cv.mod.lin.onehot <- caret::train(strength ~., data = train_data.onehot,
                           method ="lm", trControl = train.control.onehot)
#caret returns CV RMSE
cv.mod.lin.onehot $results$RMSE 

#interactions
cv.mod.int.onehot <- caret::train(strength ~.^2, data = train_data.onehot,
                           method ="lm", trControl = train.control.onehot)
cv.mod.int.onehot$results$RMSE

#quadratic
#*Looks like the code below omits any categoric variables.  Let's do that...
#*we need to pull out "method","mixing","region"
###*
###*Now that we have onehot encoding, we don't need the fancy chopjob on the population
###* Dammit! yes we do... This was a pain but now I learnt how...
###* 
quad_mod.onehot = as.formula(
  paste('strength ~',
        paste(
          paste('poly(',colnames(subset(stucco.onehot, select=-strength))
                ,',2,raw=TRUE)',
                collapse = ' + '),sep = ' + ')))


quad_mod.onehot

cv.mod.quad.onehot <- caret::train(quad_mod.onehot, data = train_data.onehot,
                            method ="lm", trControl = train.control.onehot)
cv.mod.quad.onehot$results$RMSE 




## ---------------------------------------------------------------------------
#AIC stepwise on linear model with all variables
cv.mod.lin.all.aic.onehot <- caret::train(strength ~., data = train_data.onehot,
                                   method ="lmStepAIC",trControl.onehot = train.control,trace = FALSE)
cv.mod.lin.all.aic.onehot$results$RMSE



## ---- warning=FALSE---------------------------------------------------------

#####
# regularization using ridge and lasso and elastic net
# here we will just do elasticnet since ridge/lasso are special cases
#

#grid of alpha nad lambda to consider for elastic net 
lgrid <- 10^seq(6,-3,length=25)
agrid <- seq(0,1,length=4)


#linear model
ela.lin.onehot <- caret::train(strength ~., data = train_data.onehot,
                        method = "glmnet", trControl = train.control.onehot,
                        tuneGrid = expand.grid(alpha =agrid, lambda=lgrid))
#best cv RMSE, alpha, lambda
c(min(ela.lin.onehot$results$RMSE),ela.lin.onehot$bestTune )

#interaction model
ela.int.onehot <- caret::train(strength ~.^2, data = train_data.onehot,
                        method = "glmnet", trControl = train.control.onehot,
                        tuneGrid = expand.grid(alpha =agrid, lambda=lgrid))
c(min(ela.int.onehot$results$RMSE),ela.int.onehot$bestTune )


#quadratic model
ela.quad.onehot <- caret::train(quad_mod.onehot, data = train_data.onehot,
                         method = "glmnet", trControl = train.control.onehot,
                         tuneGrid = expand.grid(alpha =agrid, lambda=lgrid))
c(min(ela.quad.onehot$results$RMSE),ela.quad.onehot$bestTune )




## ---------------------------------------------------------------------------




###---------------------------------------------------------------------------
###Let's add a random forest model just for fun.  I'm going to use the 
###onehot population.
####

## ----------------------------------------------------------------------------
set.seed(593)
#caret train Control: repeated cv
num_folds = 5
num_repeats = 3
train.control.rf <- caret::trainControl(
  method = "repeatedcv", 
  number = num_folds,
  repeats = num_repeats,
  savePredictions = TRUE,
  index = createMultiFolds(train_data.onehot$strength, k=num_folds, times=num_repeats) ,
  returnResamp = "all"
)



## ----------------------------------------------------------------------------
set.seed(13746)
#rf with caret. tuning split variable mtry
mtry.vec = 1:(dim(train_data)[2]-1)
rf.cv <- caret::train(strength ~ ., data = train_data.onehot,
                      method = "rf",
                      trControl = train.control.rf,
                      ntree=200,
                      tuneGrid = expand.grid(.mtry=mtry.vec) )




## ----------------------------------------------------------------------------
#plot validation error vs. mtry
plot(rf.cv$results$mtry,rf.cv$results$RMSE,xlab="mtry",  ylab="Validation RMSE",
     cex=1.5,pch=10,lwd=1, type="o") 

#best mtry using RMSE
rf.cv$bestTune

#final validation error
min(rf.cv$results$RMSE)

#################  END Random Forest






#summarize results from models
#

#aggregate results into data frame
cv.results = data.frame(model = c("Linear, all",
                                  "Interaction",
                                  "Quadratic",
                                  "AIC linear",
                                  "linear, elasticnet",
                                  "interaction, elasticnet",
                                  "quadratic, elasticnet",
                                  "Linear, all, onehot encoded population",
                                  "Interaction, onehot encoded population",
                                  "Quadratic, onehot encoded population",
                                  "AIC linear, onehot encoded population",
                                  "linear, elasticnet, onehot encoded population",
                                  "interaction, elasticnet, onehot encoded population",
                                  "quadratic, elasticnet, onehot encoded population",
                                  "random forest, onehot encoding"),
                        cvRMSE=c(cv.mod.lin$results$RMSE,
                                 cv.mod.int$results$RMSE,
                                 cv.mod.quad$results$RMSE,
                                 cv.mod.lin.all.aic$results$RMSE,
                                 min(ela.lin$results$RMSE),
                                 min(ela.int$results$RMSE),
                                 min(ela.quad$results$RMSE),
                                 cv.mod.lin.onehot$results$RMSE,
                                 cv.mod.int.onehot$results$RMSE,
                                 cv.mod.quad.onehot$results$RMSE,
                                 cv.mod.lin.all.aic.onehot$results$RMSE,
                                 min(ela.lin.onehot$results$RMSE),
                                 min(ela.int.onehot$results$RMSE),
                                 min(ela.quad.onehot$results$RMSE),
                                 min(rf.cv$results$RMSE)
                        )
)
#sort the models in order of performance
cv.results[order(cv.results$cvRMSE),]
cv.results[which.min(cv.results$cvRMSE),]


###*Observations:
###*
###*1.  Random forest wins hands down.
###*
###*2.  One hot encoding does improve results but not to a signifcant degree
###*
###*3.  If time permitted, I would take the more sophisticated models further
###*    (gbm, etc)


## ----------------------------------------------------------------------------
#refit on training and hten apply to test
set.seed(4987)
rf.final <- randomForest::randomForest(strength ~ . , data = train_data.onehot
                                       , ntree=200, mtry = rf.cv$bestTune[['mtry']], importance=TRUE)


###implement onehot encoding on test set

dmy <- dummyVars(" ~ .", data = test_data)
test_data.onehot <- data.frame(predict(dmy, newdata = test_data))

names(test_data.onehot)
###Looks good


## ----------------------------------------------------------------------------
#predict on test
pred.strength <- predict(rf.final,test_data.onehot)
pred.strength <- data.frame(strength=pred.strength)
pred.strength
write.csv(pred.strength,"../data/predictions.csv0", row.names = FALSE)
